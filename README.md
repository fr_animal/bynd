## Images API

### Dependencies
[Node](https://nodejs.org/en/)
[Yarn](https://yarnpkg.com/lang/en/docs/install/)
[If using Docker](https://www.docker.com/community-edition)

### Run
```bash
npm i && npm start
```
or

```bash
yarn && yarn start
```

or

```bash
docker-compose up
```

### Swagger docs

{baseURL}/docs

e.g. [http://localhost:3000/docs](http://localhost:3000/docs)
