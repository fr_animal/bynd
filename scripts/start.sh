if [[ -z "${NODE_ENV}" ]]; then
  echo "NODE_ENV not set: assuming development"
fi

if [ "$NODE_ENV" != "production" ]
then
  nodemon src --exec babel-node
fi
