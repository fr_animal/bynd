/* eslint-env jest */
import Images from './'
import imagesFixture from './fixtures/image-list.json'

describe('Images', () => {
  it('getList()', async () => {
    const imageModel = { find: jest.fn(() => Promise.resolve(imagesFixture)) }
    const imageClient = Images({ imageModel })
    const images = await imageClient.getList()
    expect(imageModel.find).toHaveBeenCalledWith({})
    expect(images).toEqual(imagesFixture)
  })

  it('getOne()', async () => {
    const [ imageFixture ] = imagesFixture
    const imageModel = { findOne: jest.fn(() => Promise.resolve(imageFixture)) }
    const imageClient = Images({ imageModel })
    const id = 124
    const image = await imageClient.getOne(id)
    expect(imageModel.findOne).toHaveBeenCalledWith({id: id})
    expect(image).toEqual(imageFixture)
  })

  it('create()', async () => {
    const [ imageFixture ] = imagesFixture
    const imageModel = { save: jest.fn(() => Promise.resolve(imageFixture)) }
    const bucket = { put: jest.fn(() => Promise.resolve()) }
    const imageClient = Images({ imageModel, bucket })
    const name = 'pic.jpg'
    const blob = 'someimagedata'
    const width = 50
    const height = 60
    const { id } = await imageClient.create(name, width, height, blob)
    expect(bucket.put).toHaveBeenCalledWith(name, blob)
    expect(imageModel.save).toHaveBeenCalledWith({ name, width, height })
    expect(id).toBe(imageFixture.id)
  })
})
