export default ({ bucket, imageModel }) => ({
  getList: () => imageModel.find({}),
  getOne: id => imageModel.findOne({id}),
  create: async (name, width, height, blob) => {
    await bucket.put(name, blob)
    const image = imageModel.save({ name, width, height })
    return image
  }
})
