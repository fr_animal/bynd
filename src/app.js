import assert from 'assert'
import express from 'express'
import swaggerTools from 'swagger-tools'
import yaml from 'js-yaml'
import fs from 'fs'
import cors from 'cors'

// #GET Images
import imagesFixture from './images/fixtures/image-list.json'
import imagesRoute from './api/images'
import images from './images'

export default () => {
  const imageModel = {
    find: () => Promise.resolve(imagesFixture),
    findOne: query => Promise.resolve(imagesFixture.find(i => i.id === query.id)),
    save: () => Promise.resolve(imagesFixture[0])
  }

  const bucket = { put: () => Promise.resolve() }

  const imageClient = images({ imageModel, bucket })

  let swaggerDoc

  try {
    swaggerDoc = yaml.safeLoad(fs.readFileSync('./swagger.yaml', 'utf8'))
  } catch (e) {
    throw new Error('Check your yaml path')
  }

  const app = express()

  app.disable('x-powered-by')
  app.use(cors())
  app.start = () => new Promise((resolve, reject) => {
    swaggerTools.initializeMiddleware(swaggerDoc, middleware => {
      app.use(middleware.swaggerMetadata())
      app.use(middleware.swaggerSecurity({
        basicAuth: (req, authOrSecDef, scopesOrApiKey, cb) => {
          try {
            const auth = req.headers.authorization.split(' ')
            let creds = Buffer.from(auth[1], 'base64')
              .toString()
              .split(':')
            assert.deepEqual(creds, ['admin', 'password'])
            cb()
          } catch (e) {
            e.statusCode = 401
            cb(e)
          }
        }
      }))
      app.use(middleware.swaggerValidator({ validateResponse: true }))
      app.use(middleware.swaggerRouter({ controllers: {
        ...imagesRoute({ imageClient })
      }}))
      app.use(middleware.swaggerUi())
      resolve()
    })
  })

  return app
}
