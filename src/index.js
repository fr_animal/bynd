import App from './app'
(async () => {
  const app = App()
  await app.start()
  const port = process.env.BYND_PORT || 3000
  app.listen(port, () => {
    console.log(`listening on ${port}`)
  })
})()
