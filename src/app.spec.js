/* eslint-env jest */
import path from 'path'

import App from './app'
import request from 'supertest'

const app = App()

describe('server', () => {
  beforeEach(async () => {
    await app.start()
  })

  const imageKeys = ['id', 'url', 'width', 'height']

  it('#GET /images should list images', async () => {
    await request(app)
      .get('/v1/images')
      .expect(200)
      .expect('Content-Type', /json/)
      .then(({ body }) => {
        body.forEach(i => {
          expect(i).toContainAllKeys(imageKeys)
        })
      })
  })

  it('#GET /image should get single image', async () => {
    await request(app)
      .get('/v1/images/345')
      .expect(200)
      .expect('Content-Type', /json/)
      .then(({ body }) => {
        expect(body).toContainAllKeys(imageKeys)
        expect(body.id).toBe(345)
      })
  })

  it('#GET /image should 404 when no image found', async () => {
    await request(app)
      .get('/v1/images/3456')
      .expect(404)
      .expect('Content-Type', /json/)
  })

  it('#POST /upload should accept images', async () => {
    await request(app)
      .post('/v1/upload')
      .auth('admin', 'password')
      .field('fileName', 'nc')
      .attach('fileData', path.resolve(__dirname, 'images', 'fixtures', 'nc.jpg'))
      .expect(201)
      .then(({ body }) => expect(body).toContainAllKeys(imageKeys))
  })
})
