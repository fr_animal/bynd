/* eslint-env jest */
import imageRoute from './images'
import imagesFixture from '../images/fixtures/image-list.json'

jest.mock('../images')

describe('images route', () => {
  it('Should send a list of images', async () => {
    const imageClient = { getList: () => Promise.resolve(imagesFixture) }
    const req = {}
    const res = { send: jest.fn() }
    await imageRoute({ imageClient }).listImages(req, res)
    expect(res.send).toHaveBeenCalledWith(imagesFixture)
  })

  it('Should get a single image', async () => {
    const [ imageFixture ] = imagesFixture
    const imageClient = { getOne: jest.fn(() => Promise.resolve(imageFixture)) }
    const req = {
      swagger: {
        params: {
          id: {
            value: 123
          }
        }
      }
    }
    const res = { send: jest.fn() }
    await imageRoute({ imageClient }).getImage(req, res)
    expect(imageClient.getOne).toHaveBeenCalledWith(req.swagger.params.id.value)
    expect(res.send).toHaveBeenCalledWith(imageFixture)
  })

  it('Should upload an image', async () => {
    const [ imageFixture ] = imagesFixture
    const imageClient = { create: jest.fn(() => Promise.resolve(imageFixture)) }
    const req = {
      swagger: {
        params: {
          fileData: {
            value: {
              buffer: 'blah'
            }
          },
          fileName: {
            value: 'nc.jpg'
          }
        }
      }
    }
    const json = jest.fn()
    const res = { status: jest.fn(() => ({json})) }
    await imageRoute({ imageClient }).uploadImage(req, res)
    expect(imageClient.create).toHaveBeenCalledWith(
      req.swagger.params.fileName.value,
      req.swagger.params.fileData.value.buffer
    )
    expect(res.status).toHaveBeenCalledWith(201)
    expect(json).toHaveBeenCalledWith(imageFixture)
  })
})
