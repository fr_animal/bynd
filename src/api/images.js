export default ({ imageClient, bucket }) => ({
  listImages: async (req, res) => {
    const images = await imageClient.getList()
    res.send(images)
  },
  getImage: async (req, res) => {
    const image = await imageClient.getOne(req.swagger.params.id.value)
    if (!image) return res.status(404).json()
    res.send(image)
  },
  uploadImage: async (req, res) => {
    const image = await imageClient.create(
      req.swagger.params.fileName.value,
      req.swagger.params.fileData.value.buffer
    )
    res.status(201).json(image)
  }
})
